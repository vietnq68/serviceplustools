import com.csvreader.CsvReader;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.bonnier.api.model.userservice.User;

import java.io.IOException;

/**
 * Created by vietnq2 on 10/21/15.
 */
public class DeleteInactiveAccount {
    private static Logger logger = LoggerFactory.getLogger(DeleteInactiveAccount.class);
    public static void main(String[] args) throws IOException {


        deleteInactiveAccount();
    }
    public static void deleteInactiveAccount() throws IOException {
        User.initialiseClient(ConfigFactory.load(),"");
        CsvReader reader = new CsvReader("src/main/resources/api_users/inactive_backoffice_accounts.csv");
        reader.readHeaders();
        while(reader.readRecord()) {
            String userId = reader.get(0);
            if(!userId.startsWith("active_")) {
                User.delete(userId);
                logger.info("deleted {}",userId);
            }
        }
    }

    private static String inactiveBOId() throws IOException {
        CsvReader reader = new CsvReader("src/main/resources/api_users/inactive_backoffice_accounts.csv");
        reader.readHeaders();
        StringBuilder sb = new StringBuilder("");
        while(reader.readRecord()) {
            String userId = reader.get(0);
            sb.append(userId).append(" OR ");
        }
        return sb.toString();
    }
}
