# Use cases for DN Bonpay





## USE CASE 1: Register and Pay
### Pre-conditions
1. User is not already registered (based on email in serviceplus)

### Main success scenario
1. End user visits DN homepage
2. User clicks on "DIGITAL PRENUMERATION"
3. Overlay appears showing two forms:
  * one to enter personal number
  * one to login
4. User enters a valid personal number and clicks "Se Ditt pris nu" to submit the form
5. Content of overlay changes to display product/price information and a "Skapa konto" and "Betala" form.
6. User enters valid first and last names, email address, password and password confirmation.
7. User enters a valid payment card number, expiry month and year and CVC/CVV number.
8. User checks the terms checkbox to confirm they agree to the terms and conditions.
9. User clicks on "Genomför beställning" to submit the form.
10. Content of overlay is replaced with the following:
  * content confirming registration and payment successful (including product description, price and payment reference number?)
  * login form
  * additional information about DN+ (iPad, Android and pdf formats)
11. Registration link ("DIGITAL PRENUMERATION") is removed from the page behind the overlay.
12. User receives an activation email with a link to activate.

### Extensions
4a1. User enters invalid personal number and click "Se ditt pris nu" to submit the form.
4a2. DN system (not serviceplus) displays error message about invalid personal number.

6a1. User does not enter valid registration values in the "Skapa konto" section of the form.
6a2. User enters a valid payment card number, expiry month and year and CVC/CVV number.
6a3. User checks the terms checkbox to confirm they agree to the terms and conditions.
6a4. User clicks on "Genomför beställning" to submit the form.
6a5. Content of overlay is replaced by form showing the "Skapa konto" section with a relevant error message and the relevant fields highlighted.
6a6. User corrects errors to proceed.

7a1. User enters invalid payment details (one or more of card number, expiry date or CVC/CVV code).
7a2. User checks the terms checkbox to confirm they agree to the terms and conditions.
7a3. User clicks on "Genomför beställning" to submit the form.
7a4. Content of overlay is replaced by form showing the "Betala" section with a relevant error message and the relevant fields highlighted.
7a5. User corrects errors to proceed.

8a1. User does not check Terms checkbox.
8a2. User clicks on "Genomför beställning" to submit the form.
8a3. Content of overlay is replaced with "Betala" section, error message saying terms must be agreed to.
8a4. User checks the Terms checkbox to proceed.

NOTE: If both 6a and 7a (and/or 8a) apply, both sections of the register and pay form are displayed with errors.

9a1: User clicks "Villkor" link to read terms and conditions.
9a2: User clicks on "Avbryt" at the bottom of the terms.
9a3: The terms content disappears to be replaced by the register and pay form and the Terms checkbox is unchecked.
NOTE: User must agree to terms to proceed.

10a1. Content of overlay is replaced with the following:
  * content saying that payment was rejected
  * link to enter an alternative card
10a2. User clicks link
10a3. "Betala" form is shown with Terms checkbox
10a4. User enters new card details to proceed

### Sub-variations
7va1. User clicks on (i) icon to view help information about CVC/CVV in "tool-tip"
7va2. User clicks on tool-tip or (i) icon to close the tool-tip.

8va1. User clicks "Villkor" link to read terms and conditions.
8va2. Terms appear in the overlay, replacing the form.
8va3. User clicks "Godkänn" to agree to terms.
8va4. Terms are replaced by form (with forms populated with values form previous steps) and the Terms checkbox is checked.

### Issues/Questions
1. Wireframes did not include product information and price above the register and pay form. These have been added to the prototype and included in the Selenium test for the main success scenario.
2. How many error messages are displayed if there are errors in both registration and payment sections of the register and pay form?
4. Wireframes did not include the display of the payment reference on the success screen. These have been added to the prototype and included in the Selenium test for the main success scenario.

### TODO
1. Write selenium tests for all extensions. A basic Selenium test for the Main Success Scenario has been written.
2. Write Selenium test for sub-variations
3. Write Selenium tests for use cases below.





## USE CASE 2: Login
### Pre-conditions
1. User is already registered

### Main success scenario
1. End user visits DN homepage
2. User clicks on "LOGGA IN"
3. The login form appears in an overlay
4. User enters a valid email and password and submits the form
5. Overlay disappears and a "LOGGA UT" link replaces "LOGGA IN"

### Extensions
4a1. User enters a valid email and password, checks the "Remember Me" checkbox and submits the form
4a2. Overlay disappears and a "LOGGA UT" link replaces "LOGGA IN"
4a3. User closes the browser
4a4. User reopens the browser and navigates to the home page
4a5. User is automatically logged in, "LOGGA UT" link shows instead of "LOGGA IN"

### Sub-variations
4va1. User enters invalid credentials
4va2. Error message displayed with login form
4va3. User enters valid credentials to proceed

6va1. User has access to DN Premium so can see content behind the paywall

6vb1. User has no access to DN Premium so is logged in but cannot see content behind the paywall





## USE CASE 3: Log out
### Pre-conditions
1. User is already logged in

### Main success scenario
1. User clicks "LOGGA UT"
2. "LOGGA UT" link is replaced by "LOGGA IN" and user does not have access to paywall content





## USE CASE 4: User changes password
### Pre-conditions
1. User is already registered

### Main success scenario
1. User clicks on "LOGGA IN"
2. The login form appears in an overlay
3. The user clicks on "Forgotten password"
4. Login form is replaced with a "Change password" form
5. User enters email and submits form
6. Overlay disappears (should display a confirmation message and login form?) and user sent an email
7. User clicks link in email
8. Browser opens with form to enter new password including confirmation
...
TODO: Change service and back office to support client's return url, styling and scripts

### Extensions
5a1. User clicks "Tillbaka till Logga in" and login form replaces forgotten password form





## USE CASE 5: Register for an account
### Pre-conditions
1. User is not already registered (based on email in serviceplus)

### Main success scenario
TODO: Identity steps and write a Selenium test for this



## USE CASE 6: User activates account
### Pre-conditions
1. User has registered but has not clicked the link in the activation email

### Main success scenario
TODO: Change service and back office to support client's return url, styling and scripts





## USE CASE 7: Existing account holder wants to pay for DN Premium
Different cases:
  * User has card registered
  * User does not have card registered

### Main success scenario
TODO: Add buy button to main pages of prototype (before after login pages)





## USE CASE 8: Order is cancelled?

### Main success scenario
TODO: Cancel button on payment form (not on register and pay)
