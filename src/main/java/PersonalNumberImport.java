import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.bonnier.api.model.userservice.ExternalSubscriber;
import se.bonnier.api.util.ObjectUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by vietnq2 on 8/12/15.
 */
public class PersonalNumberImport {
    private static String env = "prod";

    private static Logger LOG = LoggerFactory.getLogger(PersonalNumberImport.class);
    private static String S_PLUS_DATA_FOLDER ="src/main/resources/splusdata/";

    private static String OUTPUT_FOLDER = "src/main/resources/output/";
    private static String PROVIDER_DATA_FOLDER = "src/main/resources/pno/";
    private static String[] ORDER_DATA_FILES = new String[]{"orders_1.csv"};
    private static String[] ENTITLEMENT_DATA_FILES = new String[]{
            "entitlements_1.csv",
            "entitlements_2.csv",
            "entitlements_3.csv",
            "entitlements_4.csv"};
    private static String[] PROVIDER_DATA_FILES = new String[]{"PNO_DN.csv","PNO_HD.csv","PNO_SDS.csv"};

    private static Set<String> usersHavingPN = new HashSet<String>();
    private static Map<String,String> providerData = new HashMap<String, String>();

    public static void main(String[] args) throws IOException {

        S_PLUS_DATA_FOLDER += env + "/";
        OUTPUT_FOLDER += env + "/";

        prepareData();

//        processOutputFile("out_orders_1.csv");
    }

    private static void prepareData() throws IOException {
        long x= System.currentTimeMillis();

        for(String fileName : ORDER_DATA_FILES) {
            processOrderData(fileName);
        }

        for(String fileName : ENTITLEMENT_DATA_FILES) {
            processEntitlementData(fileName);
        }

        LOG.info("{} users have personal number in Order",usersHavingPN.size());

        for(String fileName : PROVIDER_DATA_FILES) {
            processProviderFile(fileName);
        }

        LOG.info("provider data size: {}",providerData.size());

        processEntitlementData("entitlements_2.csv");

        long y = System.currentTimeMillis();
        LOG.info("data preparing time: {}", y-x);
    }

    private static void processOutputFile(String fileName) throws IOException{
        CsvReader reader = new CsvReader(OUTPUT_FOLDER + fileName);
        reader.readHeaders();
        while(reader.readRecord()) {
            ExternalSubscriber subscriber = new ExternalSubscriber();
            subscriber.brandId = reader.get("brand_id");
            subscriber.userId = reader.get("user_id");
            subscriber.externalSystem = "PERSNR";
            subscriber.externalSubscriberId = reader.get("personal_number");

            ExternalSubscriber.create(subscriber);
        }
    }

    private static void processOrderData(String fileName) throws IOException{
        CsvReader reader = new CsvReader(S_PLUS_DATA_FOLDER + fileName);
        reader.readHeaders();
        CsvWriter writer = new CsvWriter(OUTPUT_FOLDER + "out_" + fileName);
        writer.writeRecord(new String[]{"user_id","personal_number","brand_id"});

        while(reader.readRecord()) {
           String personalNumber = getPNO(reader.get("adapter_parameters"));
            if(personalNumber != null
                    && !ObjectUtil.empty(reader.get("user_id"))
                    && !ObjectUtil.empty(reader.get("brand_id"))
                    && !"NULL".equals(reader.get("user_id"))
                    && !"NULL".equals(reader.get("brand_id"))) {

                writer.writeRecord(new String[]{reader.get("user_id"),reader.get("brand_id"),personalNumber});
                usersHavingPN.add(reader.get("user_id"));
            }
        }

        writer.close();
        reader.close();
    }

    private static void processEntitlementData(String fileName) throws IOException {
        CsvWriter writer = new CsvWriter(OUTPUT_FOLDER + "out_" + fileName);
        writer.writeRecord(new String[]{"user_id","personal_number","brand_id"});

        CsvReader reader = new CsvReader(S_PLUS_DATA_FOLDER + fileName);
        reader.readHeaders();

        while(reader.readRecord()) {
            String subscriberId = reader.get("external_subscriber_id");
            if(!usersHavingPN.contains(reader.get("user_id")) && providerData.containsKey(subscriberId)) {
                writer.writeRecord(new String[]{reader.get("user_id"),providerData.get(subscriberId),reader.get("brand_id")});
            }
        }

        reader.close();
        writer.close();
    }

    private static String getPNO(String params)  {
        String str = null;
        try {
            JSONObject jsonObject = (JSONObject) new JSONParser().parse(params);
            str = (String) jsonObject.get("personalNumber");
            LOG.info("found personalNumber: {}",str);
            if(!ObjectUtil.empty(str)) {
                str.trim();
                str.replace("-","");

                for(int i = 0; i < str.length();i++) {
                    if(!Character.isDigit(str.charAt(i))) {
                        return null;
                    }
                }

                if(str.length() == 12 && str.startsWith("19")) {
                    return str;
                }
                if(str.length() == 10 && !str.startsWith("19")) {
                    return str;
                }
            }
        } catch(ParseException e) {
            LOG.info("cannot not parse params: {}",params);
        }
        return str;
    }

    private static void processProviderFile(String fileName) throws IOException{
        CsvReader reader = new CsvReader(PROVIDER_DATA_FOLDER + fileName);
        reader.readHeaders();
        while(reader.readRecord()) {
            providerData.put(reader.get(0),reader.get(1));
        }
        reader.close();
    }
}
