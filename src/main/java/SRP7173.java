import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.bonnier.api.util.ObjectUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vietnq2 on 12/4/15.
 */
public class SRP7173 {
    private static Logger logger = LoggerFactory.getLogger(SRP7173.class);
    private static final int NB_OF_USERS_FILE = 4;
    private static final int NB_OF_ENTITLEMENTS_FILE = 3;

    private static Map<String,String> users;
    private static int count = 0;

    public static void main(String[] args) throws IOException {
        users = usersMap();
        for(int i = 0; i < NB_OF_ENTITLEMENTS_FILE; i++) {
            output(i);
        }
    }

    private static void output(int order) throws IOException {
        String fileName = "src/main/resources/7173/dn_subscriber_id_" + order + ".csv";
        String inputFileName = "src/main/resources/7173/entitlements" + order + ".csv";
        CsvWriter writer = new CsvWriter(fileName);
        writer.setDelimiter(';');
        writer.writeRecord(new String[]{"ExternalSubscriberId", "userid", "email"});
        CsvReader reader =  new CsvReader(inputFileName);
        reader.readHeaders();
        while(reader.readRecord()) {
            String userId = reader.get("user_id");
            String externalSubscriberId = reader.get("external_subscriber_id");
            if(goodFormat(externalSubscriberId)) {
                logger.info(String.valueOf(count++));
                writer.writeRecord(new String[]{externalSubscriberId, userId, users.get(userId)});
            }
        }
        reader.close();
        writer.close();
    }

    private static Map<String,String> usersMap() throws IOException {
        Map<String,String> usersMap = new HashMap<String,String>();
        for(int i = 0; i < NB_OF_USERS_FILE;i++) {
            String fileName = "src/main/resources/7173/users" + i + ".csv";
            CsvReader reader = new CsvReader(fileName);
            reader.readHeaders();
            while(reader.readRecord()) {
                usersMap.put(reader.get("id"),reader.get("email"));
            }
            reader.close();
        }
        return usersMap;
    }

    private static Boolean goodFormat(String externalSubscriberId) {
        return !ObjectUtil.empty(externalSubscriberId) && Character.isDigit(externalSubscriberId.charAt(0));
    }
}
