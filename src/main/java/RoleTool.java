import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.sun.corba.se.impl.orbutil.ObjectStreamClassUtil_1_3;
import com.typesafe.config.ConfigFactory;
import org.apache.log4j.spi.LoggerFactory;
import org.slf4j.Logger;
import se.bonnier.api.model.SearchParameters;
import se.bonnier.api.model.userservice.Role;
import se.bonnier.api.util.ObjectUtil;

import javax.naming.directory.SearchResult;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by vietnq2 on 9/30/15.
 */
public class RoleTool {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(RoleTool.class);

    public static void main(String[] args) throws IOException {
        Role.initialiseClient(ConfigFactory.load(),"");
        /*
        CsvReader apiReader = new CsvReader("src/main/resources/api_users/download/API.csv");
        CsvWriter apiWriter = new CsvWriter("src/main/resources/api_users/download/API_todo.csv");
        apiWriter.writeRecord(new String[]{"userId","roleId"});
        apiReader.readHeaders();
        Map<String,String> processed = new HashMap<String, String>();
        while(apiReader.readRecord()) {
            String roleName = apiReader.get("new_role").trim();
            String userId = apiReader.get("id");
            String permissions = apiReader.get("new_permission");
            permissions = permissions.trim();
            permissions = permissions.replace("\n","");
            if(!ObjectUtil.empty(roleName) && !processed.containsKey(roleName)) {
                Role role = new Role();
                role.name = roleName;
                role.permissions = Arrays.asList(permissions.split(","));

                Role newRole = Role.create(role);
                if(!newRole.hasErrors()) {
                    logger.info("created new role {}",newRole.id);

                    processed.put(roleName,newRole.id);
                    apiWriter.writeRecord(new String[]{userId, newRole.id});
                }
            } else {
                apiWriter.writeRecord(new String[]{userId,processed.get(roleName)});
            }
        }
        apiReader.close();
        apiWriter.close();
        */

        /*
        CsvReader backofficeReader = new CsvReader("src/main/resources/api_users/download/new_bo_permission.csv");
        backofficeReader.readHeaders();
        while(backofficeReader.readRecord()) {
            String roleName = backofficeReader.get("role");
            String permissions = backofficeReader.get("new_permissions");
            permissions = permissions.trim();
            permissions = permissions.replace("\n","");
            Role role = new Role();
            role.name = roleName + "_TEST_6663";
            role.permissions = Arrays.asList(permissions.split(","));
            role.description = "test SRP-6663";
            Role.create(role);
        }
        backofficeReader.close();
        */
        deleteInactiveRole();

    }

    private static void deleteTestRole() {
        SearchParameters sp = SearchParameters.getBuilder().query("name_s:*TEST_6663*").build();
        se.bonnier.api.response.SearchResult<Role> roles = Role.find(sp);
        for(Role role : roles.getItems()) {
            System.out.println(role.name);
            Role.delete(role.id);
        }
    }

    private static void deleteInactiveRole() throws IOException{
        CsvReader reader = new CsvReader("src/main/resources/api_users/inactive_role.csv");
        reader.readHeaders();
        while(reader.readRecord()) {
            String id = reader.get("id");
            Role.delete(id);
            logger.info("deleted {}",id);
        }
    }
}
