package models;

import se.bonnier.api.client.ApiErrorCode;
import se.bonnier.api.response.DateFacet;
import se.bonnier.api.response.Facet;
import se.bonnier.api.response.FacetField;
import se.bonnier.api.response.RangeFacet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 *
 * @author jonas.vis
 */
@XmlRootElement(name = "response")
@XmlAccessorType(XmlAccessType.FIELD)
public class GenericApiResponse {
    
    @XmlAttribute
    public Integer httpResponseCode;

    @XmlAttribute
    public ApiErrorCode errorCode;

    @XmlAttribute
    public String requestId;

    @XmlElement
    public String errorMsg;

    @XmlElement
    public Integer numItems;

    @XmlElement
    public Integer startIndex;

    @XmlElement
    public Long totalItems;

    @XmlElement
    public String query;
    
    @XmlElement
    public List<FacetField> facetFields;
    
    @XmlElement
    public List<Facet> facetQueries;
    
    @XmlElement
    public List<RangeFacet> rangeFacets;

    @XmlElement
    public List<DateFacet> dateFacets;
}
