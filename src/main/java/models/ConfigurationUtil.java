package models;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.bonnier.api.model.SearchParameters;
import se.bonnier.api.model.configurationservice.Configuration;
import se.bonnier.api.response.SearchResult;
import se.bonnier.api.util.ObjectUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Utils to get configuration value from ConfigurationService
 * //TODO move configuration related methods in BaseController to this class
 *
 * Created by vietnq2 on 11/26/15.
 */
public class ConfigurationUtil {
    private static Logger LOGGER = LoggerFactory.getLogger(ConfigurationUtil.class);

    private static final String CONFIGURATION_PREFIX = "feature.cfg-";

    private static final String CONFIGURATION_OBJECT_PREFIX = "feature.cfg-obj-";

    private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
    private static final TypeReference TYPE_REFERENCE = new TypeReference<Map<String, String>>() {};

    public static final String DEFAULT_STOP_WORDS = "ab, aktiebolag, hb, handelsbolag, kb, kommanditbolag, a/s, oy, -, &";

    public static Configuration getConfigurationObject(String cfgKey) {
        SearchParameters sp = SearchParameters.getBuilder()
                .query("cfgKey_sci:" + cfgKey)
                .maxResults(1)
                .sortField("updated")
                .sortOrder("desc")
                .build();
        Configuration configuration = null;

        SearchResult<Configuration> result = Configuration.find(sp);
        if (result.getItems().size() > 0) {
            configuration = result.getItems().get(0);
        }

        return configuration;
    }

    public static String getConfiguration(String cfgKey) {

        Configuration configuration = getConfigurationObject(cfgKey);

        return configuration.cfgValue;
    }

    public static Integer getIntConfiguration(String key) {
        String value = getConfiguration(key);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static List<String> getCompanyStopWords(String brandId) {
        List<String> result = new ArrayList<String>();
        String confValue = getConfiguration("stop.words.in.company.name");
        String stopWords = "";
        if(!ObjectUtil.empty(confValue)) {
            try {
                Map<String,String> pairs = JSON_MAPPER.readValue(confValue,TYPE_REFERENCE);
                stopWords = pairs.get(brandId);
            } catch(IOException e) {
                LOGGER.warn("Stop words for company is not configured in JSON string");
            }
        }
        String[] parts = stopWords.split(",");
        for(String part : parts) {
            result.add(part.trim().toLowerCase());
        }
        return result;
    }
}
