import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.*;

/**
 * Created by vietnq2 on 9/7/15.
 */
public class ApiUserTool {
    private static final Logger LOG = org.slf4j.LoggerFactory.getLogger(ApiUserTool.class);
    public static void main(String[] args) throws IOException{
//        findInactiveApiAccounts();
//        findInactiveBackOfficeAccounts();
//        countApiUser();
//        findApiUsage();
        writeNewPersmission();
    }

    private static void findInactiveApiAccounts() throws IOException{
        CsvReader splunkReader = new CsvReader("src/main/resources/api_users/apiUsers_2months_splunk.csv");
        CsvReader dbReader = new CsvReader("src/main/resources/api_users/apiUsers_db.csv");
        CsvWriter inactiveWriter = new CsvWriter("src/main/resources/api_users/inactive_accounts.csv");
        CsvWriter activeWriter = new CsvWriter("src/main/resources/api_users/active_accounts.csv");
        activeWriter.writeRecord(new String[]{"apiUserId", "email","role","permission"});
        splunkReader.readHeaders();
        dbReader.readHeaders();

        String tmp = splunkReader.getHeader(0);

        splunkReader.close();

        while(dbReader.readRecord()) {
            if(!tmp.contains(dbReader.get("id"))) {
                inactiveWriter.writeRecord(new String[]{dbReader.get("id"), dbReader.get("email")});
            } else {
                activeWriter.writeRecord(new String[]{dbReader.get("id"), dbReader.get("email"),dbReader.get("name"),dbReader.get("permissions")});
            }
        }
        dbReader.close();
        activeWriter.close();
        inactiveWriter.close();
    }

    private static void findInactiveBackOfficeAccounts() throws IOException{
        CsvReader splunkReader = new CsvReader("src/main/resources/api_users/apiUsers_2months_splunk.csv");
        CsvReader dbReader = new CsvReader("src/main/resources/api_users/backoffice_accounts.csv");
        CsvWriter inactiveWriter = new CsvWriter("src/main/resources/api_users/inactive_backoffice_accounts.csv");
        CsvWriter activeWriter = new CsvWriter("src/main/resources/api_users/active_backoffice_accounts.csv");
        activeWriter.writeRecord(new String[]{"apiUserId", "email"});
        splunkReader.readHeaders();
        dbReader.readHeaders();

        String tmp = splunkReader.getHeader(0);

        LOG.info("splunk list: {}",tmp);

        splunkReader.close();

        while(dbReader.readRecord()) {
            if(!tmp.contains(dbReader.get("id"))) {
                inactiveWriter.writeRecord(new String[]{dbReader.get("id"), dbReader.get("email")});
            } else {
                activeWriter.writeRecord(new String[]{dbReader.get("id"), dbReader.get("email")});
            }
        }
        dbReader.close();
        activeWriter.close();
        inactiveWriter.close();
    }

    private static String getSearchApiUserString() throws IOException {
        CsvReader reader = new CsvReader("src/main/resources/api_users/active_backoffice_accounts.csv");
        StringBuilder sb = new StringBuilder("");
        int count = 0;
        reader.readHeaders();
        while(reader.readRecord()) {
            if(count == 0) {
                sb.append("\"").append(reader.get("apiUserId")).append("\"");
            } else {
                sb.append(" OR ").append("\"").append(reader.get("apiUserId")).append("\"");
            }
            count++;
        }
        reader.close();
        return sb.toString();
    }

    private static void findApiUsage() throws IOException{
        CsvReader usageReader = new CsvReader("src/main/resources/api_users/api_accounts_usage.csv");
        CsvReader activeAccountReader = new CsvReader("src/main/resources/api_users/active_accounts.csv");

        Map<String,Set<Usage>> usage = new HashMap<String, Set<Usage>>();
        Map<String,Account> accounts = new HashMap<String, Account>();
        usageReader.readHeaders();
        activeAccountReader.readHeaders();
        while(activeAccountReader.readRecord()) {
            accounts.put(activeAccountReader.get("apiUserId"),new Account(activeAccountReader.get("apiUserId"),
                    activeAccountReader.get("email"),
                    activeAccountReader.get("role"),
                    activeAccountReader.get("permission")));
            usage.put(activeAccountReader.get("apiUserId"), new HashSet<Usage>());
        }

        while(usageReader.readRecord()) {
            usage.get(usageReader.get("apiUserId")).add(new Usage(usageReader.get("method"),usageReader.get("path")));
        }


        usageReader.close();
        activeAccountReader.close();

        CsvWriter writer = new CsvWriter("src/main/resources/api_users/summary_api_accounts.csv");
        writer.writeRecord(new String[]{"apiUserId","role","permission","usage"});
        for(String key : usage.keySet()) {
            for(Usage tmp : usage.get(key)) {
                Account account = accounts.get(key);
                writer.writeRecord(new String[]{key,account.email,account.role,account.permissions,tmp.method + " " + tmp.path});
            }
        }
        writer.close();
    }

    private static void writeNewPersmission() throws IOException{
        CsvReader reader = new CsvReader("src/main/resources/api_users/summary_api_accounts.csv");
        reader.readHeaders();
        Map<String,Set<String>> tmp = new HashMap<String, Set<String>>();

        Map<String,Account> accounts = new HashMap<String, Account>();
        while(reader.readRecord()) {

            accounts.put(reader.get("apiUserId"),new Account(reader.get("apiUserId"),
                    reader.get("email"),
                    reader.get("role"),
                    reader.get("permissions")));
            String apiUserId = reader.get("apiUserId");
            String permission = reader.get("new_permission");
            if(permission != null && permission.length() > 0) {
                if(tmp.get(apiUserId) == null) {
                    tmp.put(apiUserId,new HashSet<String>());
                    tmp.get(apiUserId).add(permission);
                } else {
                    tmp.get(apiUserId).add(permission);
                }
            }
        }
        reader.close();
        CsvWriter writer = new CsvWriter("src/main/resources/api_users/out/final_api_accounts.csv");
        CsvWriter tmpwriter = new CsvWriter("src/main/resources/api_users/out/details_final_api_accounts.csv");
        writer.writeRecord(new String[]{"id", "email", "old_role", "old_permission", "new_permission"});
        for(String key:tmp.keySet()) {
            Account account = accounts.get(key);
            writer.writeRecord(new String[]{key,account.email,account.role,account.permissions,permissionFromSet(tmp.get(key))});
            for(String perm : tmp.get(key)) {
                tmpwriter.writeRecord(new String[]{key,perm});
            }
        }
        writer.close();
        tmpwriter.close();
    }

    private static String permissionFromSet(Set<String> set) {
        int i = 0;
        StringBuilder sb = new StringBuilder("");
        for(String perm : set) {
            if(i==0) {
                sb.append(perm);
            } else {
                sb.append(",").append(perm);
            }
            i++;
        }
        return sb.toString();
    }
}


