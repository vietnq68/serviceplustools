import com.csvreader.CsvReader;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import se.bonnier.api.model.SearchParameters;
import se.bonnier.api.model.entitlementservice.Entitlement;
import se.bonnier.api.model.userservice.User;

import javax.naming.directory.SearchResult;
import java.io.IOException;

/**
 * Created by vietnq2 on 8/27/15.
 */
public class DeleteTestUser  {
    private static Logger LOG = org.slf4j.LoggerFactory.getLogger(DeleteTestUser.class);
    public static void main(String[] args) throws IOException{
        Config config = ConfigFactory.load();
        User.initialiseClient(config,"");
        Entitlement.initialiseClient(config, "");

        SearchParameters sp = SearchParameters.getBuilder().query("id:" + "08XY6DKTJxKVpTj6ICUtTy").build();
        se.bonnier.api.response.SearchResult<Entitlement> result = Entitlement.find(sp);

        CsvReader entReader = new CsvReader("src/main/resources/splusdata/todelete/entitlement_1.csv");
        CsvReader userReader = new CsvReader("src/main/resources/splusdata/todelete/user_1.csv");

        entReader.readHeaders();
        userReader.readHeaders();
        /*
        while(entReader.readRecord()) {
            Entitlement.delete(entReader.get(entReader.get("id")),"3kG8N41hrM1MSudx2TpVQW");
        }
        String tmp = "";
        while(userReader.readRecord()) {
            User.delete(userReader.get("id"),"3kG8N41hrM1MSudx2TpVQW");
            tmp+="'" + userReader.get("id") + "',";
        }
        */

        entReader.close();
        userReader.close();
    }
}
