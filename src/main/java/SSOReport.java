import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import models.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vietnq2 on 1/7/16.
 */
public class SSOReport {
    private Map<String,User> usersFromFile(String fileName) throws IOException {
        Map<String,User> result = new HashMap<String,User>();
        CsvReader reader = new CsvReader(fileName);
        reader.readHeaders();
        while(reader.readRecord()) {
            result.put(reader.get("email"), new User(reader.get("id"),
                    reader.get("email"),
                    reader.get("fist_name"),
                    reader.get("last_name"),
                    reader.get("password"),
                    reader.get("phone_number")));
        }
        reader.close();
        return  result;
    }

    private void compareAndWriteFile(Map<String, User> dnUsers,Map<String,User> diUsers) throws IOException {
        CsvWriter writer = new CsvWriter("");
        writer.writeRecord(new String[]{"email","di_id","di_first_name","di_last_name","di_phone_number","dn_id","dn_first_name","dn_last_name","dn_phone_number"});
        for(String email : dnUsers.keySet()) {
            User dnUser = dnUsers.get(email);
            if(diUsers.containsKey(email)) {
                User diUser = diUsers.get(email);
                if(!dnUser.firstName.equals(diUser.firstName)
                        || !dnUser.lastName.equals(diUser.lastName)
                        || !dnUser.phoneNumber.equals(dnUser.phoneNumber)) {
                    writer.writeRecord(new String[] {email,
                            diUser.id,diUser.firstName,diUser.lastName,diUser.phoneNumber,
                            dnUser.id,dnUser.firstName,dnUser.lastName,dnUser.phoneNumber
                    });
                }
            }
        }
        writer.close();
    }
}
