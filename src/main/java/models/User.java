package models;

/**
 * Created by vietnq2 on 1/7/16.
 */
public class User {
    public String id;
    public String email;
    public String firstName;
    public String lastName;
    public String password;
    public String phoneNumber;

    public User(String id, String email, String firstName, String lastName, String password,String phoneNumber) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password = password;
    }
}
