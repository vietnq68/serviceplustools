import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import java.io.IOException;
import java.util.*;

/**
 * Created by vietnq on 9/16/15.
 */
public class BackOfficeUserTool {
    public static void main(String[] args) throws IOException{
        writeBOPermissions();
        finalBOAccount();
    }

    private static void writeBOPermissions() throws IOException{
        CsvReader reader = new CsvReader("src/main/resources/api_users/active_backoffice_accounts.csv");
        reader.readHeaders();
        List<String> activeAccounts = new ArrayList<String>();
        while(reader.readRecord()) {
            activeAccounts.add(reader.get("apiUserId"));
        }
        reader.close();

        reader = new CsvReader(("src/main/resources/api_users/backoffice_account_with_role.csv"));
        reader.readHeaders();
        Map<String,Account> map = new HashMap<String, Account>();
        Map<String,String> rolePerm = new HashMap<String, String>();
        while(reader.readRecord()) {
            String apiUserId = reader.get("id");
            if(activeAccounts.contains(apiUserId)) {
                map.put(apiUserId,new Account(apiUserId,reader.get("email"),reader.get("name"),reader.get("permissions")));
                rolePerm.put(reader.get("name"),reader.get("permissions"));
            }
        }
        reader.close();

        reader = new CsvReader("src/main/resources/api_users/backoffice_usage.csv");
        reader.readHeaders();
        Map<String,Set<String>> tmp = new HashMap<String, Set<String>>();
        while(reader.readRecord()) {
            String apiUserId = reader.get("apiUserId");
            String usage = reader.get("method") + " "  + reader.get("path");
            if(map.get(apiUserId) != null) {
                String role = map.get(apiUserId).role;
                if(tmp.get(role) == null) {
                    tmp.put(role, new HashSet<String>());
                    tmp.get(role).add(usage);
                } else {
                    tmp.get(role).add(usage);
                }
            }
        }
        reader.close();
        CsvWriter writer = new CsvWriter("src/main/resources/api_users/summary_backoffice_account.csv");
        writer.writeRecord(new String[]{"role","old_permission","permission"});
        for(String key:tmp.keySet()) {
            for(String usage : tmp.get(key)) {
                writer.writeRecord(new String[]{key,rolePerm.get(key), permFromUsage(usage)});
            }
        }
        writer.close();

    }

    private static String permFromUsage(String usage) {
        String tmp1[] = usage.split(" ");

        String method = tmp1[0];
        String path = tmp1[1];
        if(tmp1.length > 2) {
            method = tmp1[1];
            path = tmp1[2];
        }
        String[] tmp = path.split("/");
        String rootPath = tmp[0];
        rootPath = rootPath.replaceAll("-","");
        if(rootPath.endsWith("s")) {
            rootPath = rootPath.substring(0,rootPath.length() - 1);
        }
        String perm = "";
        switch(tmp.length) {
            case 1:
                if("GET".equals(method)) {
                    perm = "api:" + rootPath + ":find";
                }
                if("POST".equals(method)) {
                    perm = "api:" + rootPath + ":create";
                }
                if("PUT".equals(method)) {
                    perm = "api:" + rootPath + ":update";
                }
                if("DELETE".equals(method)) {
                    perm = "api:" + rootPath + ":delete";
                }
                break;
            case 2:
                perm = "api:" + rootPath + ":" + tmp[1];
                break;
            case 3:
                perm = "api:" + rootPath + ":" + tmp[2];
                break;
            default:
                perm = "job-scheduling";
        }
        return perm;
    }

    private static String stringFromSet(Set<String> set) {
        int i = 0;
        StringBuilder sb = new StringBuilder("");
        for(String perm : set) {
            if(i==0) {
                sb.append(perm);
            } else {
                sb.append(",").append(perm);
            }
            i++;
        }
        return sb.toString();
    }

    private static void finalBOAccount() throws IOException {
        CsvReader reader = new CsvReader("src/main/resources/api_users/summary_backoffice_account.csv");
        CsvWriter writer = new CsvWriter("src/main/resources/api_users/out/final_backoffice_accounts.csv");
        CsvWriter tmpwriter = new CsvWriter("src/main/resources/api_users/out/details_final_backoffice_accounts.csv");
        reader.readHeaders();
        writer.writeRecord(new String[]{"role","old_permissions","new_permissions"});
        Map<String,Set<String>> map = new HashMap<String, Set<String>>();
        while(reader.readRecord()) {
            String role = reader.get("role");
            String permission = reader.get("permission");
            if(map.get(role) == null) {
                map.put(role,new HashSet<String>());
                map.get(role).add(permission);
            } else {
                map.get(role).add(permission);
            }

        }
        reader.close();
        reader = new CsvReader("src/main/resources/api_users/summary_backoffice_account.csv");
        reader.readHeaders();
        Set<String> roles = new HashSet<String>();
        while(reader.readRecord()){
            String role = reader.get("role");
            if(!roles.contains(role)) {
                writer.writeRecord(new String[]{role,reader.get("old_permission"),stringFromSet(map.get(role))});
                for(String perm : map.get(role)) {
                    tmpwriter.writeRecord(new String[]{role,perm});
                }
            }
            roles.add(role);

        }
        writer.close();
        tmpwriter.close();
    }


}
