import com.typesafe.config.ConfigFactory;
import org.apache.log4j.spi.LoggerFactory;
import org.slf4j.Logger;
import se.bonnier.api.model.userservice.User;
import se.bonnier.api.oauth.OAuth;
import se.bonnier.api.oauth.OAuthSession;

/**
 * Created by vietnq2 on 9/16/15.
 */
public class Account {
    private static Logger logger = org.slf4j.LoggerFactory.getLogger(Account.class);
    public String id;
    public String role;
    public String permissions;
    public String email;

    public Account(String id,String email,String role, String permissions) {
        this.id = id;
        this.role = role;
        this.permissions = permissions;
        this.email = email;
    }

    public static void main(String[] args) {
        User.initialiseClient(ConfigFactory.load(),"");
        OAuth oAuth = new OAuth("https://api.bonnier.se/v1/oauth");
        OAuthSession seesio = oAuth.invalidateAllAccessTokens("7vGcYnIJTKoAt6IVtzKhRt","1wxYL0hQs4to8mhQzJSUfk");
        System.out.println("1");
    }
}
