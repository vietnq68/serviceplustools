import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import com.typesafe.config.ConfigFactory;
import jdk.nashorn.internal.runtime.regexp.joni.Config;
import models.ConfigurationUtil;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;
import org.codehaus.jackson.type.TypeReference;
import se.bonnier.api.client.ApiErrorCode;
import se.bonnier.api.client.bip.BipAccountApiClient;
import se.bonnier.api.client.bip.BipClient;
import se.bonnier.api.client.bip.OAuth2;
import se.bonnier.api.client.bip.OAuth2Response;
import se.bonnier.api.client.userservice.RoleApiClient;
import se.bonnier.api.model.ApiClientFactory;
import se.bonnier.api.model.SearchParameters;
import se.bonnier.api.model.configurationservice.Configuration;
import se.bonnier.api.model.entitlementservice.Product;
import se.bonnier.api.model.lookup.OrganizationLookup;
import se.bonnier.api.model.userservice.BipAccount;
import se.bonnier.api.model.userservice.Brand;
import se.bonnier.api.model.userservice.MergingInfo;
import se.bonnier.api.model.userservice.Role;
import se.bonnier.api.model.userservice.User;
import se.bonnier.api.response.GenericApiResponse;
import se.bonnier.api.response.SearchResult;
import se.bonnier.api.util.ObjectUtil;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vietnq2 on 9/3/15.
 */
public class Test {

    public static void main(String[] args) throws IOException, NoSuchFieldException, IllegalAccessException {

//        BipAccount.initialiseClient(ConfigFactory.load(), "");
//        MergingInfo info = BipAccount.getMergingInfo("sso1@di.se", "123456a", "di.se");
//        System.out.println("return");
//        String[] test = new String[]{"abc","def"};
//        List<String> testLIst = new ArrayList<String>(Arrays.asList(test));
//        testLIst.add("test");
//        System.out.println(testLIst.toString());

        String[] test = new String[]{"1","2"};
        List<String> listTest = new ArrayList<String>(Arrays.asList(test));
        listTest.add("3");
        int newSize = test.length + 1;
        String[] clone = Arrays.copyOf(test,newSize);
        clone[newSize - 1] = "3";
        for(String str : clone) {
            System.out.println(str);

        }
    }

    private void nothing() {
        /*HashMap<String, String[]> pagePermission = movePageImpl.getPermission();
        // Remove source permission
        Iterator<Map.Entry<String, String[]>> pagePermissionIterator = pagePermission.entrySet().iterator();
        while (pagePermissionIterator.hasNext()) {
            Map.Entry<String, String[]> permissionEntry = pagePermissionIterator.next();
            if (!permissionEntry.getKey().equals(movePageImpl.getAuthor())) {
                pagePermissionIterator.remove();
            }
        } */

        /*
        Map<String,String[]> pagePermissions = movePageImpl.getPermission();
        Map<String,String[]> updatedPagePermissions = new HashMap<String,String[]>();
        for(String key : pagePermissions.keySet()) {
            if(!key.equals(movePageImpl.getAuthor())) {
                updatedPagePermissions.put(key, pagePermissions.get(key));
            }
        }
        movePageImpl.setPermission(updatedPagePermissions);
        */
    }



}


